<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('getPaciente', 'HomeController@getPaciente');
Route::get('getInsumo', 'HomeController@getInsumo');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('insumos/getStock', 'InsumosController@getStock')->name('insumos/getStock');
Route::get('insumos/getProductoSelect', 'InsumosController@getProductoSelect')->name('insumos/getProductoSelect');
Route::get('insumos/getProducto', 'InsumosController@getProducto')->name('insumos/getProducto');
Route::get('insumos/createEasy', 'InsumosController@createEasy')->name('insumos/createEasy');
Route::post('insumos/storeEasy', 'InsumosController@storeEasy')->name('insumos/storeEasy');
Route::get('insumos/getProductoCbInsumo', 'InsumosController@getProductoCbInsumo')->name('insumos/getProductoCbInsumo');
Route::resource('insumos', 'InsumosController');
Route::get('insumos/destroy/{id}', 'InsumosController@destroy');
Route::get('getPaquete/{id}', 'ProcedimientoController@getPaquete');
Route::resource('procedimientos', 'ProcedimientoController');
