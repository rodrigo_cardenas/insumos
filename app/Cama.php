<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use OwenIt\Auditing\Contracts\Auditable;

class Cama extends Model implements Auditable
{
    protected $table = 'hos_cama';
	public $timestamps = false;

    public $fillable = [
        'id_estado_cama',
        'created_at',
        'updated_at',
    ];

    use \OwenIt\Auditing\Auditable;

    public function sala(){
        return $this->belongsTo('App\Sala', 'id_sala');
    }

    public function estado_cama(){
        return $this->belongsTo('App\EstadoCama', 'id_estado_cama')->withDefault();
    }
    
    public function tipoCama(){
        return $this->belongsTo('App\TipoCama', 'id_tipo_cama')->withDefault();
    }
    
    public function unidadFuncional(){
        return $this->belongsTo('App\UnidadFuncional', 'id_unidad_funcional')->withDefault();
    }

    public function ingreso(){
        return $this->hasOne('App\Ingreso', 'tx_cama')->withDefault();
    }

    public function getTienePacienteAttribute(){
        return ($this->ingreso->paciente_id != null ? 1 : 0);
    }

    public function getServicioAttribute(){
        return $this->sala->unidad->id_servicio;
    }

    public function subUnidad(){
        return $this->belongsTo('App\SubUnidad', 'id_sub_unidad')->withDefault();
    }

    public function turnosMedicosCama()
    {
        return $this->hasMany('App\TurnoMedicoCama', 'id_cama');
    }
    
    public function movimientos()
    {
        return $this->hasMany('App\Traslado', 'id_cama');
    }
}
