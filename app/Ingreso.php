<?php

namespace App;
use Carbon\Carbon;
use App\Escala;
use App\Procedencia;
use App\Unidad;
use Auth;


use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model 
{
    public $table = 'hos_ingresos';
    protected $connection = 'mysql2';
    protected $appends = ['fecha_ingreso'];

    public $fillable = [
    	'paciente_id',
        'hospitalizacion_id',
        'tutor_id',
        'fc_ingreso_servicio',
        'fc_ingreso_hospital',
        'tx_sector',
        'tx_sala',
        'tx_cama',
        'tx_funcionario_entrega',   
        'tx_procedencia',
        'tx_servicio_responsable',
        'tx_estado_conciencia',
        'tx_estado_siquico',
        'tx_descripcion_ingreso',
        'tx_diagnostico_ingreso',
        'tx_antecedentes_morbidos',
        'tx_alergia',
        'tx_descripcion_alergia',
        'tx_quirurgicos',
        'tx_med_uso_hab',
        'tx_dispositivos',
        'tx_lenguaje',
        'tx_higiene',
        'tx_respiracion',
        'tx_oxigenoterapia',
        'tx_piel',
        'tx_audicion',
        'tx_audifonos',
        'tx_audifonos_ubicacion',
        'tx_vision',
        'tx_anteojos',
        'tx_protesis',
        'tx_protesis_ubicacion',
        'tx_estado_nutricional',
        'tx_cabeza',
        'tx_pupilas',
        'tx_cuello',
        'tx_yugular',
        'tx_torax',
        'tx_abdomen',
        'tx_genitales',
        'tx_eess',
        'tx_eess_otros',
        'tx_eeii',
        'tx_eeii_otros',
        'tx_observaciones_fisicas',
        'nr_frecuencia_respiratoria',
        'nr_frecuencia_cardiaca',
        'nr_presion_sistolica',
        'nr_presion_diastolica',
        'nr_presion_media',
        'nr_temperatura',
        'nr_saturacion',
        'nr_peso',
        'nr_talla',
        'nr_imc',
        'tx_bra_percepcion',
        'tx_bra_nutricion',
        'tx_bra_movilidad',
        'tx_bra_humedad',
        'tx_bra_cutanea',
        'tx_bra_actividad',
        'tx_bra_resultado',
        'tx_riesgo_caida_percepcion',
        'tx_riesgo_caida_deficit',
        'tx_riesgo_caida_medicamento',
        'tx_riesgo_caida_mental',
        'tx_riesgo_caida_deambulacion',
        'tx_riesgo_caida_resultado',
        'tx_glasgow_ocular',
        'tx_glasgow_verbal',
        'tx_glasgow_motora',
        'tx_glasgow_resultado',
        'nr_ba_lavarseBaño',
        'nr_ba_arreglarseAseo',
        'nr_ba_vestirse',
        'nr_ba_comer',
        'nr_ba_retrete',
        'nr_ba_trasladarse',
        'nr_ba_deambular',
        'nr_ba_escalones',
        'nr_ba_miccion',
        'nr_ba_deposiciones',
        'tx_resultado_barthel',
        'tx_eva_escala',
        'tx_sas_escala',
        'tx_neurobloqueo',
        'tx_examenes',
        'tx_radiologicos',
        'tx_por_tomar',
        'tx_pendientes',
        'tx_medicamentos',
        'tx_pertenencias',
        'tx_observaciones',
        'tx_equipo_medico',
        'tx_responsable',
        'estado',
        'id_usuario'
    ];

    
    public function getEdadIngresoAttribute(){
        if ($this->fc_nacimiento != null) {
            $f1 = Carbon::createFromFormat('Y-m-d', $this->fc_nacimiento);
            $f2 = Carbon::createFromFormat('Y-m-d', $this->fc_ingreso_servicio);
            $anos = $f2->diff($f1)->format('%y años');
            // solo si es menor de 3 años mostrar años con meses y días
            if ($f2->diff($f1)->y < 3) {
                $dias = $f2->diff($f1)->format('%d días');
                $meses = $f2->diff($f1)->format('%m meses');
                $anos = $f2->diff($f1)->format('%y años');

                if($f2->diff($f1)->format('%d') == '1')
                    $dias = $f2->diff($f1)->format('%d día');
                if($f2->diff($f1)->format('%m') == '1')
                    $meses = $f2->diff($f1)->format('%m mes');
                if($f2->diff($f1)->format('%y') == '1')
                    $anos = $f2->diff($f1)->format('%y año');

                if ($f2->diff($f1)->y == 0) {
                    if ($f2->diff($f1)->format('%m') == '0'){
                        return $dias;
                    } 
                    return "{$meses} y {$dias}";
                }
                
                return "{$anos} con {$meses} y {$dias}";
            }
            return $anos;
        }
        return false;
    }

    public function ingresos_medicos()
    {
        // IngresoMedico::where('id_hospitalizacion','=',$ingreso->hospitalizacion_id)->get()->last();
        return $this->hasMany('App\IngresoMedico', 'id_hospitalizacion', 'hospitalizacion_id');
    }
    
    public function ruq()
    {
        return $this->hasMany('App\Ruq', 'ingreso_id', 'id');
    }
    
    public function categorizaciones()
    {
        // Categorizacion::where('hos_ingreso_enfermeria_id','=',$ingreso->id)->get()->last();
        return $this->hasMany('App\Categorizacion', 'hos_ingreso_enfermeria_id');
    }

    public function getUltimaCategorizacionAttribute(){
        return ( isset($this->categorizaciones) ? $this->categorizaciones->last() : null);
    }
    
    // Relacion antigua, esta no es la correcta pero se deja para no afectar otras funciones que la ocupan
    public function traslados()
    {
        return $this->hasMany('App\Traslado', 'id_hospitalizacion', 'hospitalizacion_id');
    }
    // Relacion actual, entra en vigencia desde el 04-09-2019, ya que desde ese día se asocian la totalidad de movimientos a un ingreso
    public function movimientos()
    {
        return $this->hasMany('App\Traslado', 'id_ingreso', 'id');
    }

    public function getUltimoMovimientoAttribute(){
        return ( isset($this->traslados) ? $this->traslados->last() : null);
    }
    
    public function getPrimerMovimientoAttribute(){
        return ( isset($this->traslados) ? $this->traslados->first() : null);
    }

    public function turnos()
    {
        return $this->hasMany('App\Turno', 'hos_ingreso_enfermeria_id');
    }
    
    public function turnosMedicosCamas()
    {
        return $this->hasMany('App\TurnoMedicoCama', 'id_ingreso');
    }
    
    public function getUltimoTurnoMedicoAttribute()
    {
        return ( isset($this->turnosMedicosCamas) ? $this->turnosMedicosCamas->last() : null);
    }
    
    public function getUltimoTurnoAttribute()
    {
        return ( isset($this->turnos) ? $this->turnos->last() : null);
    }

    public function usuario()
    {
		return $this->belongsTo('App\User', 'id_usuario')->withDefault();
    }
    
    public function procedencia()
    {
		return $this->belongsTo('App\Procedencia', 'tx_procedencia')->withDefault();
    }
    
    public function hospitalizacion()
    {
        return $this->belongsTo('App\Hospitalizacion', 'hospitalizacion_id');
    }
	
    public function servicio()
    {
		return $this->belongsTo('App\Servicio', 'tx_servicio_responsable', 'cd_servicio')->withDefault();
	}

    public function unidad()
    {
		return $this->belongsTo('App\Unidad', 'tx_sector')->withDefault(["tx_descripcion" => "No asignada"]);
	}

    public function sala()
    {
		return $this->belongsTo('App\Sala', 'tx_sala')->withDefault(["tx_sala" => "No asignada"]);
	}

    public function cama()
    {
		return $this->belongsTo('App\Cama', 'tx_cama')->withDefault(["cd_cama" => ""]);
    }
    
    public function epcrisisEnfermeria()
    {
		return $this->belongsTo('App\Epicrisisenf', 'id', 'ingreso_id')->withDefault();
    }
    
    public function epcrisisGinecologica()
    {
		return $this->belongsTo('App\EpicrisisGine', 'id', 'ingreso_id')->withDefault();
    }
    
    public function getUlimoRuqDiagnosticoAttribute()
    {
        $ruq = $this->ruq->last();
        return (isset($ruq->tx_diagnostico)) ? $ruq->tx_diagnostico : '';
    }
    
    public function getUlimoRuqPendientesAttribute()
    {
        $ruq = $this->ruq->last();
        return (isset($ruq->tx_pendientes)) ? $ruq->tx_pendientes : '';
    }

    public function getFechaCreacionAttribute()
    {
		return ($this->created_at != null) ? Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d-m-Y H:i') : 'N/A';
	}
    
    public function getFechaIngresoAttribute()
    {
		return ($this->fc_ingreso_servicio != null) ? Carbon::createFromFormat('Y-m-d H:i:s', $this->fc_ingreso_servicio)->format('d-m-Y H:i') : 'N/A';
    }


    public function getFechaHospitalizacionAttribute()
    {
		return ($this->fc_ingreso_hospital != null) ? Carbon::createFromFormat('Y-m-d H:i:s', $this->fc_ingreso_hospital)->format('d-m-Y H:i') : "No tiene";
    }

    public function getDiasHospitalizadoAttribute()
    {
        return ($this->fc_ingreso_hospital != null ? Carbon::parse($this->fc_ingreso_hospital, 'America/Santiago')->diffInDays(Carbon::now('America/Santiago')) : null);
    }
    
    public function getDiasServicioAttribute()
    {
        return ($this->fc_ingreso_servicio != null ? Carbon::parse($this->fc_ingreso_servicio, 'America/Santiago')->diffInDays(Carbon::now('America/Santiago')) : null);
    }

    //si han pasado más de 24 horas retornar true
    public function getBloqueaEditAttribute()
    {
        if (Auth::user()->servicio_id === 86) {
            return ($this->fc_ingreso_servicio != null ? Carbon::parse($this->fc_ingreso_servicio, 'America/Santiago')->diffInHours(Carbon::now('America/Santiago')) > 120 : null );
        }
        return ($this->fc_ingreso_servicio != null ? Carbon::parse($this->fc_ingreso_servicio, 'America/Santiago')->diffInHours(Carbon::now('America/Santiago')) > 24 : null );
    }
    

    public function paciente()
    {
        return $this->belongsTo('App\Paciente', 'paciente_id')->withDefault();
    }

    public function diagnostico()
    {
        return $this->belongsTo('App\Diagnostico', 'tx_diagnostico_ingreso', 'cd_diagnostico')->withDefault(["tx_descripcion" => "Sin información", "cd_diagnostico" => "Sin información"]);
    }

    public function equipo()
    {
        return $this->belongsTo('App\Equipo', 'tx_equipo_medico', 'tx_codigo')->withDefault();
    }

    public function getEquipoMedicoAttribute()
    {
        $equipo = $this->equipo;
        return ($equipo->tx_descripcion != null ? $equipo->tx_descripcion : "No tiene");
    }

    public function dispositivos_ingreso()
    {
        return $this->hasMany('App\DispositivoIngreso','id_ingreso_enfermeria');
    }

    public function getEessAttribute()
    {
        return rtrim(semicolon2Colon($this->tx_eess), ", ");
    }

    public function getEeiiAttribute()
    {
        return rtrim(semicolon2Colon($this->tx_eeii), ", ");
    }

    public function getPielAttribute()
    {
        return rtrim(semicolon2Colon($this->tx_piel), ", ");
    }

    public function getEstadoConcienciaAttribute()
    {
        return rtrim(semicolon2Colon($this->tx_estado_conciencia), ", ");
    }

    public function getNeurobloqueoAttribute()
    {
        return $this->tx_neurobloqueo == 1 ? "SI" : "NO";
    }

    public function getEscalasAttribute()
    {
        $escalasArray = [
            "braden_percepcion" => new Escala(["cd_valor" => $this->tx_bra_percepcion, 'cd_tipo_escala' => 'bra', 'cd_escala' => 'per']),
            "braden_nutricion" => new Escala(["cd_valor" => $this->tx_bra_nutricion, 'cd_tipo_escala' => 'bra', 'cd_escala' => 'nut']),
            "braden_movilidad" => new Escala(["cd_valor" => $this->tx_bra_movilidad, 'cd_tipo_escala' => 'bra', 'cd_escala' => 'mov']),
            "braden_humedad" => new Escala(["cd_valor" => $this->tx_bra_humedad, 'cd_tipo_escala' => 'bra', 'cd_escala' => 'hum']),
            "braden_cutanea" => new Escala(["cd_valor" => $this->tx_bra_cutanea, 'cd_tipo_escala' => 'bra', 'cd_escala' => 'cut']),
            "braden_actividad" => new Escala(["cd_valor" => $this->tx_bra_actividad, 'cd_tipo_escala' => 'bra', 'cd_escala' => 'act']),
            "glasgow_ocular" => new Escala(["cd_valor" => $this->tx_glasgow_ocular, 'cd_tipo_escala' => 'gla', 'cd_escala' => 'ocu']),
            "glasgow_verbal" => new Escala(["cd_valor" => $this->tx_glasgow_verbal, 'cd_tipo_escala' => 'gla', 'cd_escala' => 'ver']),
            "glasgow_motora" => new Escala(["cd_valor" => $this->tx_glasgow_motora, 'cd_tipo_escala' => 'gla', 'cd_escala' => 'mot']),
            "barthel_lavarse_baño" => new Escala(["cd_valor" => $this->nr_ba_lavarseBaño, 'cd_tipo_escala' => 'bar', 'cd_escala' => 'lav']),
            "barthel_aseo" => new Escala(["cd_valor" => $this->nr_ba_arreglarseAseo, 'cd_tipo_escala' => 'bar', 'cd_escala' => 'aseo']),
            "barthel_vestirse" => new Escala(["cd_valor" => $this->nr_ba_vestirse, 'cd_tipo_escala' => 'bar', 'cd_escala' => 'ves']),
            "barthel_comer" => new Escala(["cd_valor" => $this->nr_ba_comer, 'cd_tipo_escala' => 'bar', 'cd_escala' => 'com']),
            "barthel_retrete" => new Escala(["cd_valor" => $this->nr_ba_retrete, 'cd_tipo_escala' => 'bar', 'cd_escala' => 'ret']),
            "barthel_trasladarse" => new Escala(["cd_valor" => $this->nr_ba_trasladarse, 'cd_tipo_escala' => 'bar', 'cd_escala' => 'tra']),
            "barthel_deambular" => new Escala(["cd_valor" => $this->nr_ba_deambular, 'cd_tipo_escala' => 'bar', 'cd_escala' => 'deam']),
            "barthel_escalones" => new Escala(["cd_valor" => $this->nr_ba_escalones, 'cd_tipo_escala' => 'bar', 'cd_escala' => 'esc']),
            "barthel_miccion" => new Escala(["cd_valor" => $this->nr_ba_miccion, 'cd_tipo_escala' => 'bar', 'cd_escala' => 'mic']),
            "barthel_deposiciones" => new Escala(["cd_valor" => $this->nr_ba_deposiciones, 'cd_tipo_escala' => 'bar', 'cd_escala' => 'dep']),
		];
		$escalas = Escala::whereIn('cd_tipo_escala', ['bra', 'gla', 'bar'])->get();

		foreach ($escalasArray as $escala) {
            $escalaSeleccionada = $escalas
            ->where('cd_tipo_escala', $escala->cd_tipo_escala)->where('cd_escala', $escala->cd_escala)->where('cd_valor', $escala->cd_valor)->first();
            $escala["tx_descripcion"] = $escalaSeleccionada != null ? $escalaSeleccionada->tx_descripcion : null;
            $escala["id"] = $escalaSeleccionada != null ? $escalaSeleccionada->id : null;
		}
		return $escalasArray;
    }

    /**
     * Obtener la escala asociada a la columna cd_valor
     *
     * @param  string $tipo_escala
     * @param  string $cd_escala
     * @param  int $cd_valor
     * @return \Illuminate\Database\Eloquent\Model\Escala
     */
    public function escalaCDValor($tipo_escala, $cd_escala, $cd_valor)
    {
        return $cd_valor != 0 ? Escala::where('cd_tipo_escala', $tipo_escala)->where('cd_escala', $cd_escala)->where('cd_valor', $cd_valor)->first() : new Escala;
    }

    /**
     * Obtener la escala asociada a la columna id, si no encuentra retorna un modelo vacío de Escala
     *
     * @param int $valor
     * @return \Illuminate\Database\Eloquent\Model\Escala
     */
    public function escalaID($valor)
    {
        return is_int($valor) ? Escala::find($valor) : new Escala;
    }


    public function getRiesgoCaidaPercepcionAttribute()
    {
        return $this->tx_riesgo_caida_percepcion == 1 ? "SI" : "NO";
    }

     /**
     * Obtener la descripcion del riesgo caida deficit
     *
     * @param void $this->tx_riesgo_caida_deficit
     * @return string $descripcion
     */
    public function getRiesgoCaidaDeficitAttribute()
    {
        if($this->tx_riesgo_caida_deficit == null || $this->tx_riesgo_caida_deficit == 0){ return null; }

        $ids = stringToArray($this->tx_riesgo_caida_deficit);
        $descripcion = "";
        
        foreach ($ids as $id) {
            $descripcion .= "{$this->escalaID(intval($id))->tx_descripcion}, ";
        }
        return rtrim($descripcion, ", ");
    }


    /**
     * Obtener la descripcion del riesgo caida medicamento
     *
     * @param void $this->tx_riesgo_caida_medicamento
     * @return string $descripcion
     */
    public function getRiesgoCaidaMedicamentoAttribute()
    {
        if($this->tx_riesgo_caida_medicamento == null || $this->tx_riesgo_caida_medicamento == 0){ return null; }
        
        $ids = stringToArray($this->tx_riesgo_caida_medicamento);
        $descripcion = "";
        foreach ($ids as $id) {
            $descripcion .= "{$this->escalaID(intval($id))->tx_descripcion}, ";
        }
        return rtrim($descripcion, ", ");
    }

    public function getEstadoMentalAttribute()
    {
        return $this->tx_riesgo_caida_mental == 0 ? "Orientado" : "Confuso";
    }

    public function upp()
    {
        return $this->hasMany('App\Upp', 'id_hosp_ingreso_enfermeria')->whereNull('fc_elimina_upp');
    }

    public function getDispositivosOldAttribute()
    {
        if($this->tx_dispositivos == null ){ return null; }
        
        $ids = stringToArray($this->tx_dispositivos);
        $dispositivos = collect();
        foreach ($ids as $id) {
            $dispositivoIngreso = new DispositivoIngreso;
            $dispositivoIngreso->id_dispositivo = $id;
            $dispositivos->push($dispositivoIngreso);
        }
        return $dispositivos;
    }

    public function getProcedenciaPacienteAttribute()
    {
        if ($this->tx_procedencia == null ):
            return "Desconocido";
        endif;
        $procedencia = $this->procedencia;
        if ($procedencia->id != null):
            return $procedencia->tx_procedencia_paciente;
        else:
            $procedencia = Unidad::find($this->tx_procedencia);
            return $procedencia != null ? $procedencia->tx_descripcion : "Desconocido";
        endif;
    }

    public function getTieneAlergiaAttribute()
    {
        return $this->tx_descripcion_alergia != "" ? "SI" : "NO";
    }

    public function EvaluacionRiesgoPaciente()
    {
        return $this->hasMany('App\EvaluacionRiesgoPaciente', 'id_ingreso');
    }

    public function getEpicrisisMedicaOld()
    {
        if ( $this->tx_equipo_medico != 1 and $this->tx_equipo_medico != 7 ) {
            $epicrises = $this->paciente->epicrisis_medica_old_general->where('bo_borrador', 0)->where('bo_eliminado', 0);
        } else {
            $epicrises = $this->paciente->epicrisis_medica_old_vascular->where('bo_borrador', 0)->where('bo_eliminado', 0);
        }
        $fecha_ingreso = Carbon::parse($this->fc_ingreso_servicio)->subDays(2)->format('Y-m-d H:i:s');

        return $epicrises != null && $epicrises->count() > 0 ? $epicrises->filter(function($value, $key) use ($fecha_ingreso)  {
            return $value->fc_registro >= $fecha_ingreso && $value->fc_registro <= Carbon::now()->format('Y-m-d H:i:s');
        }) : [];
    }

    public function getResultadoBarthelAttribute()
    {
        $barthel = explode(" ", $this->tx_resultado_barthel);
        return intval(substr($barthel[count($barthel) - 1], 1, -1));
    }
}
