<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $connection = 'mysql3';
    protected $table = 'generales.gen_servicio';

    public function salas()
    {
        return $this->hasManyThrough('App\Sala', 'App\Unidad', 'id_servicio', 'id_unidad');
    }
}
