<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPortal extends Model
{
    protected $connection = 'mysql3';
    protected $table = 'generales.users';
    public $timestamps = false;
}
