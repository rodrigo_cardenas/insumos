<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidad extends Model
{
    // protected $connection = 'mysql2';

    protected $table = 'generales.gen_unidad';
    public $timestamps = false;

    public function sala(){
        return $this->hasMany('App\Sala', 'id_unidad', 'id');
    }

    public function subunidad(){
        return $this->hasMany('App\SubUnidad', 'id_unidad', 'id');
    }

    public function servicio()
    {
        return $this->belongsTo('App\Servicio', 'id_servicio')->withDefault(["tx_descripcion" => "No asignado"]);
    }

    public function traslados()
    {
        return $this->hasMany('App\Traslado', 'id_unidad');
    }

    public function camas()
    {
        return $this->hasManyThrough('App\Cama', 'App\Sala', 'id_unidad', 'id_sala');
    }

    public function ingresos()
    {
        return $this->hasMany('App\Ingreso', 'tx_sector');
    }
    
    public function categorizaciones()
    {
        return $this->hasManyThrough('App\Categorizacion', 'App\Ingreso', 'tx_sector', 'hos_ingreso_enfermeria_id');
    }
    
    public function movimientos()
    {
        return $this->hasMany('App\Traslado', 'id_unidad');
    }
    
    public function preHospitalizaciones()
    {
        return $this->hasMany('App\PreHospitalizacion', 'id_unidad');
    }

    

}
