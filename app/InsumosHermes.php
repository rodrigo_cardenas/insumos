<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsumosHermes extends Model
{
    protected $connection = 'mysql3';
    protected $table = 'generales.gen_insumo';
}
