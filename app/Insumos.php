<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\SoftDeletes;

class Insumos extends Model
{
    protected $appends = ['fecha_creacion'];
    public $fillable = [
        "id_paciente",
        "id_ingreso",
        "id_insumo",
        "created_by",
        "updated_by",
        "updated_at",
        "deleted_at",
    ];

    use SoftDeletes;

    public static function boot()
    {
        parent::boot();
       
        static::saving(function ($curacion) {
            $curacion->updated_by = Auth::id();
        });
        
        static::saved(function ($curacion) {
            $curacion->updated_by = Auth::id();
        });

        static::creating(function ($curacion) {
            $curacion->created_by = Auth::id();
        });
        
        static::created(function ($curacion) {
            $curacion->created_by = Auth::id();
        });
    }

    public function getPacienteNombreAttribute()
    {
        return $this->id_paciente != null ? Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y H:i:s') : null;
    }

    public function getFechaCreacionAttribute()
    {
        return $this->created_at != null ? Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y H:i:s') : null;
    }

    public function usuario()
    {
        return $this->belongsTo('App\User', 'created_by')->withDefault();
    }

    public function paciente()
    {
        return $this->belongsTo('App\Paciente', 'id_paciente')->withDefault();
    }
    
    public function insumoHermes()
    {
        return $this->belongsTo('App\InsumosHermes', 'id_insumo', 'gl_codigo_articulo')->withDefault();
    }
    
    public function ingreso()
    {
        return $this->belongsTo('App\Ingreso', 'id_ingreso')->withDefault();
    }
    
    public function scopeMasDemandado($query)
    {
        return $query->with('insumoHermes')
                     ->groupBy('id_insumo')
                     ->selectRaw('id_insumo, sum(cantidad) as cantidadTotal')
                     ->orderBy('cantidadTotal', 'desc');
    }
    
    public function scopeMasRepetido($query)
    {
        return $query->with('paciente')
                     ->select('id_paciente')
                     ->groupBy('id_paciente')
                     ->orderByRaw('COUNT(*) DESC');
    }
}
