<?php

namespace App;
use DB;
use Carbon\Carbon;

use App\Protocolo;
use App\SolicitudPabellon;
use Illuminate\Database\Eloquent\Model;

class Hospitalizacion extends Model
{
	public $timestamps = false;
	protected $connection = 'mysql2';
    protected $table = 'hos_hospitalizacion';
    protected $appends = ['dias_hospitalizado', 'fecha_creacion', 'fecha_egreso', 'fecha_hospitalizacion', 'estado_hospitalizacion', 
    'servicio_egreso', 'dias_servicio', 'dias_hospitalizacion' ];

    public $fillable = [
        "id_paciente",
        "fc_ingreso_hospitalizacion",
        "id_prevision",
        "id_clasificacion_fonasa",
        "id_servicio",
        "id_unidad",
        "id_procedencia",
        "id_estado",
        "id_usuario",
        "tx_episodio_trak",
        "id_servicio_egreso",
        "id_establecimiento_egreso"
    ];

    public function getFechaCreacionAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d-m-Y H:i');
    }
    
    public function getFechaEgresoAttribute()
    {
        return ($this->fc_egreso_hospitalizacion != null) ? Carbon::createFromFormat('Y-m-d H:i:s', $this->fc_egreso_hospitalizacion)->format('d-m-Y H:i') : 'No Egresado';
    }
    
    public function getFechaAltaAttribute()
    {
        return ($this->fc_egreso_hospitalizacion != null) ? Carbon::createFromFormat('Y-m-d H:i:s', $this->fc_egreso_hospitalizacion)->format('d-m-Y') : 'No Egresado';
    }
    
    public function getHoraAltaAttribute()
    {
        return ($this->fc_egreso_hospitalizacion != null) ? Carbon::createFromFormat('Y-m-d H:i:s', $this->fc_egreso_hospitalizacion)->format('H:i:s') : 'No Egresado';
    }
    
    public function getFechaHospitalizacionAttribute(){
        return ($this->fc_ingreso_hospitalizacion != null) ? Carbon::createFromFormat('Y-m-d H:i:s', $this->fc_ingreso_hospitalizacion)->format('d-m-Y H:i') : 'No Egresado';
    }

    public function getUltimoPartoAttribute()
    {
        return $this->partos->last();
    }

    public function getCuantosEgresosAttribute()
    {
        return $this->traslados->where('tx_tipo_movimiento', 'E')->count();
    }

    public function getUltimoIngresoAttribute()
    {
            return ( isset($this->ingresos) ? $this->ingresos->last() : null);
    }

    public function getPrimerIngresoAttribute()
    {
        return ( isset($this->ingresos) ? $this->ingresos->first() : null);
    }

    public function getEgresoAttribute()
    {
        return ( isset($this->traslados) ? $this->traslados->where('tx_tipo_movimiento', 'E')->last() : null);
    }

    public function getEpicrisisMedicaOldAttribute()
    {
        if ( $this->UltimoIngreso ) {
            if ($this->UltimoIngreso->tx_equipo_medico != 1 and $this->UltimoIngreso->tx_equipo_medico != 7) {
                $epicrises = $this->paciente->epicrisis_medica_old_general->where('bo_borrador', 0)->where('bo_eliminado', 0);
            } else {
                $epicrises = $this->paciente->epicrisis_medica_old_vascular->where('bo_borrador', 0)->where('bo_eliminado', 0);
            }
            $fecha_ingreso = $this->fc_ingreso_hospitalizacion;
            $fecha_egreso = $this->fc_egreso_hospitalizacion;

            return $epicrises != null && $epicrises->count() > 0 ? $epicrises->filter(function($value) use ($fecha_ingreso, $fecha_egreso)  {
                return $value->fc_registro >= $fecha_ingreso && $value->fc_registro <= $fecha_egreso;
            })->last() : null;

        }
        return null;
    }

    //cantidad de días que estuvo hospitalizado, fecha de egreso - fecha de ingreso hospitalización
    public function getDiasHospitalizadoAttribute()
    {
        $fc_ingreso = Carbon::createFromFormat('Y-m-d H:i:s', $this->fc_ingreso_hospitalizacion);
        $fc_egreso = $this->fc_egreso_hospitalizacion != null ? Carbon::createFromFormat('Y-m-d H:i:s', $this->fc_egreso_hospitalizacion) : null;
        return $fc_egreso != null ? $fc_egreso->diffInDays($fc_ingreso) : 0;
    }
    
    // cantidad de días que lleva en la hospitalización activa, fecha actual - fecha ingreso hospitalización
    public function getDiasHospitalizacionAttribute()
    {
        $fc_ingreso = Carbon::parse($this->fc_ingreso_hospitalizacion, 'America/Santiago');
        $hoy = Carbon::now('America/Santiago');
        return $fc_ingreso != null ? $fc_ingreso->diffInDays($hoy) : 0;
    }

    public function getEstadoHospitalizacionAttribute()
    {
        return $this->fc_egreso_hospitalizacion != null ? "Egresado" : "Activa";
    }
    
    public function getDiasServicioAttribute()
    {
        $ultimo_ingreso = $this->ingresos->last(); 
        $fc_ingreso = $ultimo_ingreso != null && $ultimo_ingreso->fc_ingreso_servicio != null ? Carbon::parse($ultimo_ingreso->fc_ingreso_servicio, 'America/Santiago') : null;
        return $fc_ingreso != null ? $fc_ingreso->diffInDays(Carbon::now('America/Santiago')) : null;
    }

    public function getServicioEgresoAttribute()
    {
        if ($this->fc_egreso_hospitalizacion == null) {
            return "No egresado";
        } else {
            $traslados = $this->traslados->where('tx_tipo_movimiento', 'E')->first();
            
            if ($traslados == null) {
                return "No asignado";
            }
            return $traslados->servicio_emite->tx_descripcion;
        }
    }

    public function getProtocolos($solicitudes_pabellon = [])
    {
        if ( count($solicitudes_pabellon) > 0 ) {
            $fecha_egreso = $this->fc_egreso_hospitalizacion === null ? Carbon::now()->format('Y-m-d H:i:s') : $this->fc_egreso_hospitalizacion;
            $fecha_entrada = Carbon::createFromFormat('Y-m-d H:i:s', $this->fc_ingreso_hospitalizacion)->format("Y-m-d"); 
            foreach ($solicitudes_pabellon as $solicitud) {
                $protocolos = $solicitud->protocolos->filter(function ($value, $key) use ($fecha_entrada, $fecha_egreso) {
                    return $value->fc_entrada_pabellon >= $fecha_entrada && $value->fc_entrada_pabellon <= $fecha_egreso;
                });
            }

            return $protocolos != null && $protocolos->count() > 0 ? $protocolos : [];
        } else {
            return [];
        }
    }

    public function getTxDestinoEgresoAttribute()
    {
        if ($this->fc_egreso_hospitalizacion == null || $this->id_destino_egreso == null) {
            return "No egresado";
        }
        if ($this->id_destino_egreso == 13 || $this->id_destino_egreso == 14) {
            return "{$this->destinoEgreso->tx_destino_egreso}, {$this->establecimiento_egreso->tx_descripcion}";
        } else if ($this->id_destino_egreso == 19) {
            return "{$this->destinoEgreso->tx_destino_egreso}, {$this->servicio_egreso_urgencia->tx_descripcion}";
        } else {
            return "{$this->destinoEgreso->tx_destino_egreso}";
        }
    }

    public function getEdadEgresoAttribute()
    {
        $paciente = $this->paciente;
        if ($paciente->fc_nacimiento != null) {
            $f1 = Carbon::createFromFormat('Y-m-d', $paciente->fc_nacimiento);
            $f2 = Carbon::createFromFormat('Y-m-d H:i:s', $this->fc_egreso_hospitalizacion);
            $anos = $f2->diff($f1)->format('%y años');
            // solo si es menor de 3 años mostrar años con meses y días
            if ($f2->diff($f1)->y < 3) {
                $dias = $f2->diff($f1)->format('%d días');
                $meses = $f2->diff($f1)->format('%m meses');
                $anos = $f2->diff($f1)->format('%y años');

                if($f2->diff($f1)->format('%d') == '1')
                    $dias = $f2->diff($f1)->format('%d día');
                if($f2->diff($f1)->format('%m') == '1')
                    $meses = $f2->diff($f1)->format('%m mes');
                if($f2->diff($f1)->format('%y') == '1')
                    $anos = $f2->diff($f1)->format('%y año');

                if ($f2->diff($f1)->y == 0) {
                    if ($f2->diff($f1)->format('%m') == '0'){
                        return $dias;
                    } 
                    return "{$meses} y {$dias}";
                }
                
                return "{$anos} con {$meses} y {$dias}";
            }
            return $anos;
        }
        return null;
    }

    public function getDiagnosticoTextoLibreAttribute()
    {

        return $this->tx_diagnostico_texto_libre != null ? 
        preg_replace( '/</' , 'menor ', $this->tx_diagnostico_texto_libre) : "Sin información"; 
    }

    public function servicio_egreso_urgencia()
    {
        return $this->belongsTo('App\Servicio', 'id_servicio_egreso')->withDefault();
    }

    public function servicio()
    {
        return $this->belongsTo('App\Servicio', 'id_servicio')->withDefault();
    }

    public function unidad()
    {
        return $this->belongsTo('App\Unidad', 'id_unidad')->withDefault(["tx_descripcion" => "No asignada"]);
    }
        
    public function usuario()
    {
        return $this->belongsTo('App\User', 'id_usuario')->withDefault();
    }

    public function paciente()
    {
        return $this->belongsTo('App\Paciente', 'id_paciente')->withDefault();
    }

    public function condicionEgreso()
    {
        return $this->belongsTo('App\CondicionEgreso', 'id_condicion_egreso')->withDefault(["tx_condicion_egreso" => "No egresado"]);
    }

    public function destinoEgreso()
    {
        return $this->belongsTo('App\DestinoEgreso', 'id_destino_egreso')->withDefault();
    }

    public function preHospitalizacion()
    {
        return $this->belongsTo('App\PreHospitalizacion', 'id_pre_hospitalizacion')->withDefault();
    }
    
    public function lactancia()
    {
        return $this->belongsTo('App\Lactancia', 'id_lactancia')->withDefault();
    }

    public function ingresos()
    {
        return $this->hasMany('App\Ingreso', 'hospitalizacion_id', 'id');
    }

    public function traslados()
    {
        return $this->hasMany('App\Traslado', 'id_hospitalizacion', 'id');
    }

    public function partos()
    {
        return $this->hasMany('App\Parto', 'id_hospitalizacion');
    }

    public function establecimiento_egreso()
    {
        return $this->belongsTo('App\Establecimiento', 'id_establecimiento_egreso')->withDefault();
    }
    
    public function pariente_egreso()
    {
        return $this->belongsTo('App\Parentesco', 'id_pariente_egreso')->withDefault();
    }
  
    public function epicrisisEnfermeria()
    {
        return $this->hasOne('App\Epicrisisenf', 'hospitalizacion_id')->withDefault();
    }

    public function evoluciones_medicas()
    {
        return $this->hasMany('App\EvolucionMedica', 'id_hospitalizacion');
    }
    
    public function ingreso_medico()
    {
        return $this->hasMany('App\IngresoMedico', 'id_hospitalizacion');
    }

    public function observaciones_oirs()
    {
        return $this->hasOne('App\ObservacionesOirs', 'id_hospitalizacion')->withDefault(["tx_observaciones" => "Sin observaciones"]);
    }

    public function epicrisis_medica_nuevo()
    {
        return $this->hasMany('App\EpicrisisMedica', 'id_hospitalizacion');
    }

    public function epicrisis_gine()
    {
        return $this->hasMany('App\EpicrisisGine', 'hospitalizacion_id');
    }
   
    public function calcula_dias_servicio($ingreso)
    {
        $fc_ingreso = $ingreso->fc_ingreso_servicio != null ? Carbon::parse($ingreso->fc_ingreso_servicio, 'America/Santiago') : null;
        return $fc_ingreso != null ? $fc_ingreso->diffInDays(Carbon::now('America/Santiago')) : null;
    }

    
}
