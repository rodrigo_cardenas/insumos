<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;

class WSIntersystem
{
    protected $baseUri = 'http://192.34.60.24/logistica/api';

    public function getProductos(Request $request)
    {
        $postdata = '{
            "tokenSeguridad":"hsjdd_articulo_paquete",
            "centroCosto" : "'.$request->centroCosto.'",
            "articuloPaquete":"'.$request->search.'"
            }';
            
        $options = array('http' =>
            array(
                'method'  => 'GET',
                'header' => "Content-type: application/json\r\n" ,
                'content' => $postdata,
            )
        );

        $context  = stream_context_create($options);
        $url = $this->baseUri."/ws-articulos-paquetes";
        $insumos = json_decode(file_get_contents($url, false, $context));
        $data['results'] = array();

        foreach ($insumos->resultado as $key => $insumo) {
            $producto = array();
            $producto['id'] = $insumo->codigo;
            $stock = isset($insumo->stock) ? $insumo->stock : '';
            $producto['text'] = "{$insumo->nombre} ({$insumo->codigo}) stock: {$stock}"; 

            array_push($data['results'], $producto);
        }

        // $data['pagination'] = ["more" => true];
        return $data;
    }

    public function getProducto(Request $request) 
    {
        $postdata = '{
            "tokenSeguridad":"hsjdd_articulo_paquete",
            "subCentroCosto" : "'.$request->centroCosto.'",
            "articuloPaquete":"'.$request->codigo.'"
            }';
        $options = array('http' =>
            array(
                'method'  => 'GET',
                'header' => "Content-type: application/json\r\n" ,
                'content' => $postdata,
            )
        );
        $context  = stream_context_create($options);
        $url = $this->baseUri."/ws-dato-articulo-paquete";
        $json = json_decode(file_get_contents($url, false, $context));
        $insumo = collect($json->resultado);
        if (!isset($insumo)) {
            $res['error'] = true;
            $res['data'] = null;
            $res['message'] = 'El producto no esta registrado';
        }
        else{
            $res['data'] = $insumo;
        }
        return $res;
    }

    public function descontarProductos(Request $request, $paciente, $ingreso)
    {
        $arrayArticulos = '';

        foreach ($request->insumos as $key => $insumo) {
            $indice = $key+1;
            $arrayArticulos .= '"articulo'.$indice.'":{
                            "articulo":"'.$insumo .'",
                            "cantidad":"'.$request->cantidad[$key] .'"
                        },';
        }

        $arrayArticulos = trim($arrayArticulos, ',');

        $postdata = '{
            "tokenSeguridad":"hsjdd_articulo_paquete",
            "tipoIdentificacion":"1",
            "identificacionPaciente":"'.$paciente->nr_run.'",
            "digitoVerificador":"'.$paciente->tx_digito_verificador.'",
            "nombres":"'.$paciente->tx_nombre.'",
            "apellidoPaterno":"'.$paciente->tx_apellido_paterno.'",
            "apellidoMaterno":"'.$paciente->tx_apellido_materno.'",
            "fechaNacimiento":"'.$paciente->fc_nacimiento.'",
            "genero":"'.$paciente->id_sexo.'",
            "prevision":"'.$paciente->id_prevision.'",
            "tipoAtencion":"1",
            "evento":"'.$ingreso->hospitalizacion_id.'",
            "subCentroCosto":"'.$request->id_unidad.'",
            "arrayArticulos":{
                            '.$arrayArticulos.'
            }
        }';

        $options = array('http' =>
            array(
                'method'  => 'GET',
                'header' => "Content-type: application/json\r\n" ,
                'content' => $postdata,
            )
        );

        $context  = stream_context_create($options);
        $url = $this->baseUri."/ws-consumo-articulo";
        $json = json_decode(file_get_contents($url, false, $context));

        return $json;
    }
    
    public function descontarProducto(Request $request, $paciente, $ingreso)
    {
        $arrayArticulos = '';
        foreach ($request->prod as $key => $insumo) {
            if($key==0)
                continue;
            $indice = $key+1;
            $arrayArticulos .= '"articulo'.$indice.'":{
                            "articulo":"'.$insumo .'",
                            "cantidad":"1"
                        },';
        }
        $arrayArticulos = trim($arrayArticulos, ',');

        $postdata = '{
            "tokenSeguridad":"hsjdd_articulo_paquete",
            "tipoIdentificacion":"1",
            "identificacionPaciente":"'.$paciente->nr_run.'",
            "digitoVerificador":"'.$paciente->tx_digito_verificador.'",
            "nombres":"'.$paciente->tx_nombre.'",
            "apellidoPaterno":"'.$paciente->tx_apellido_paterno.'",
            "apellidoMaterno":"'.$paciente->tx_apellido_materno.'",
            "fechaNacimiento":"'.$paciente->fc_nacimiento.'",
            "genero":"'.$paciente->id_sexo.'",
            "prevision":"'.$paciente->id_prevision.'",
            "tipoAtencion":"1",
            "evento":"'.$ingreso->hospitalizacion_id.'",
            "subCentroCosto":"298",
            "arrayArticulos":{
                            '.$arrayArticulos.'
            }
        }';
        $options = array('http' =>
            array(
                'method'  => 'GET',
                'header' => "Content-type: application/json\r\n" ,
                'content' => $postdata,
            )
        );
        $context  = stream_context_create($options);
        $url = $this->baseUri."/ws-consumo-articulo";
        $json = json_decode(file_get_contents($url, false, $context));

        return $json;
    }

    public function devolverProducto($insumo)
    {
        $arrayArticulos = '"articulo1":{
            "articulo":"'.$insumo->id_insumo .'",
            "cantidad":"'.$insumo->cantidad .'"
        }';

        $postdata = '{
            "tokenSeguridad":"hsjdd_articulo_paquete",
            "tipoIdentificacion":"1",
            "identificacionPaciente":"'.$insumo->paciente->nr_run.'",
            "digitoVerificador":"'.$insumo->paciente->tx_digito_verificador.'",
            "prevision":"'.$insumo->paciente->id_prevision.'",
            "tipoAtencion":"1",
            "evento":"'.$insumo->ingreso->hospitalizacion_id.'",
            "subCentroCosto":"'.$insumo->ingreso->tx_sector.'",
            "arrayArticulos":{
                            '.$arrayArticulos.'
            }
        }';

        $options = array('http' =>
            array(
                'method'  => 'GET',
                'header' => "Content-type: application/json\r\n" ,
                'content' => $postdata,
            )
        );

        $context  = stream_context_create($options);
        $url = "http://192.34.60.24/logistica/api/ws-devolucion-consumo-articulo";
        $json = json_decode(file_get_contents($url, false, $context));

        return $json;
    }

}