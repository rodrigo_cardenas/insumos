<?php

namespace App\Http\Controllers;

use App\Procedimiento;
use Illuminate\Http\Request;

class ProcedimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $procedimientos = Procedimiento::with('paquetes.insumo')->get();
        return view('procedimientos.index', compact('procedimientos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('procedimientos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nuevoProcedimiento = new Procedimiento;
        $nuevoProcedimiento->tx_descripcion = $request->tx_descripcion;
        $nuevoProcedimiento->save();

        foreach ($request->insumos as $key => $insumo) {
            if (!$request->cantidad[$key])
                continue;
            $array_insumos[$key]['id_insumo'] = $insumo;
            $array_insumos[$key]['cantidad'] = $request->cantidad[$key];
        }

        $nuevoProcedimiento->paquetes()->createMany($array_insumos);

        return redirect(action('ProcedimientoController@index'))->with('message', 'Ha registrado correctamente el procedimiento');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Procedimiento  $procedimiento
     * @return \Illuminate\Http\Response
     */
    public function show(Procedimiento $procedimiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Procedimiento  $procedimiento
     * @return \Illuminate\Http\Response
     */
    public function edit(Procedimiento $procedimiento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Procedimiento  $procedimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Procedimiento $procedimiento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Procedimiento  $procedimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Procedimiento $procedimiento)
    {
        $deleteInsumosProcedimiento = $procedimiento->paquetes()->delete();
        $deleteProcedimiento = $procedimiento->delete();

        if (!$deleteProcedimiento || !$deleteInsumosProcedimiento) {
            return redirect(action('ProcedimientoController@index'))->with('error', "No se ha eliminado el registro");
        }

        return redirect(action('ProcedimientoController@index'))->with('message', "Usted ha eliminado correctamente el registro");
    }

    public function getPaquete($id)
    {
        $procedimientos = Procedimiento::with('paquetes.insumo')->find($id);
        return view('procedimientos.listaPaquetes', compact('procedimientos'));;
    }
}
