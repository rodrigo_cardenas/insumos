<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\UserPortal;
use App\User;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function redirectTo()
    {
        return url('insumos/create');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

     /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        // return response()->json(["status" => "OK"]);
        return view('auth.login');
    }



     /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm(Request $request)
    {
        Auth::logout();
        $rut = $request->input('rUser');
        if (isset($rut)) {
            $this->authenticate($request);
            if ($request->hospitalizacion_id != null) {
                return redirect(url("home"));
            } else {
                return redirect(url('home'));
            }
        } else {
            return view('auth.login');
        }
    }


    public function authenticate(Request $request)
    {
        if ($request->rUser != null) {
            $rut = $request->rUser;
            $token = $request->tUser;
            $userPortal = UserPortal::where('rut', $rut)->firstOrFail();
            if ($userPortal->token === $token) {
                $user = User::where('rut', $rut)->firstOrFail();
                Auth::loginUsingId($user->id);
                Session::put('token', $token);
                return true;
            } else {
                return false;       
            }
        } else {
            $credentials = $request->only('rut', 'password');
            if (Auth::attempt($credentials)) {
                // Authentication passed...
                return redirect()->intended('dashboard');
            }
        }
    }


    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'rut';
    }
}
