<?php

namespace App\Http\Controllers;

use App\Ingreso;
use App\Insumos;
use App\Paciente;
use App\CbInsumos;
use App\Procedimiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\InsumosRequest;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Api\WSIntersystem;

class InsumosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $insumos = Insumos::with('usuario', 'paciente', 'insumoHermes', 'ingreso.hospitalizacion.unidad')->orderBy('id', 'desc')->get()->take(100);
        $response = Response::json(['data'=>$insumos],200);
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $ficha = isset($request->ficha) ? $request->ficha : '';
        $procedimientos = Procedimiento::select('id', 'tx_descripcion as text')->get(); 
        return view('insumos.create', compact('ficha', 'procedimientos'));
    }

    /**
     * Show the form for creating a new resource (adaptado para lector de codigos de barra).
     *
     * @return \Illuminate\Http\Response
     */
    public function createEasy()
    {
        return view('insumos.create_easy');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsumosRequest $request, WSIntersystem $client)
    {
        if (!empty($request->id_paciente) && isset($request->insumos)) {

            $ingreso = Ingreso::find($request->id_ingreso);
            $paciente = (!empty($request->nr_run)) ? Paciente::where('nr_run', substr($request->nr_run, 0, -2))->get()->last() : Paciente::where('nr_ficha', $request->ficha)->get()->last();
            
            // ws de descuento
            $json = $client->descontarProductos($request, $paciente, $ingreso);

            if (empty($json) || $json->codigo == '009') {
                return redirect()->back()->withErrors("Ha ocurrido el siguiente error: {$json->mensaje}")->withInput();
            }
            
            // registro de insumos en nuestra BD
            foreach ($request->insumos as $key => $insumo) {
                $nuevoInsumo = new Insumos;
                $nuevoInsumo->id_paciente = $paciente->id;
                $nuevoInsumo->id_ingreso = $request->id_ingreso;
                $nuevoInsumo->id_insumo = $insumo;
                $nuevoInsumo->cantidad = $request->cantidad[$key];
                $nuevoInsumo->save();
            }

            return redirect(route('home'))->with('message', 'Ha registrado correctamente los insumos');
        } else {
            return redirect()->back()->withErrors('Antes debe asociar un paciente')->withInput();
        }
    }

    /**
     * Store a newly created resource in storage (adaptado para lector de codigos de barra).
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeEasy(Request $request, WSIntersystem $client)
    {
        if (!empty($request->id_paciente) ) {

            $ingreso = Ingreso::find($request->id_ingreso);
            $paciente = Paciente::where('nr_ficha', $request->ficha)->get()->last();
            
            // ws de descuento
            $json = $client->descontarProductos($request, $paciente, $ingreso);

            if (empty($json) || $json->codigo == '009') {
                return redirect()->back()->withErrors("Ha ocurrido el siguiente error: {$json->mensaje}")->withInput();
            }

            // registro de insumos en nuestra BD
            foreach ($request->prod as $key => $insumo) {
                if($key==0)
                    continue;
                
                $nuevoInsumo = new Insumos;
                $nuevoInsumo->id_paciente = $paciente->id;
                $nuevoInsumo->id_ingreso = $request->id_ingreso;
                $nuevoInsumo->id_insumo = $insumo;
                $nuevoInsumo->save();
            }
            return redirect(route('home'))->with('message', 'Ha registrado correctamente los insumos');
        } else {
            return redirect()->back()->withErrors('Antes debe asociar un paciente')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Insumos  $insumos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Insumos $insumos, $id, WSIntersystem $client)
    {
        $insumo = Insumos::find($id);
        
        // ws de descuento
        $json = $client->devolverProducto($insumo);
        
        if ($json->mensaje != '001') {
            return redirect(route('home'))->with('error', "{$json->mensaje}");
        }elseif (!$insumo->delete()) {
            return redirect(route('home'))->with('error', "No se ha eliminado el registro");
        }

        return redirect(route('home'))->with('message', "Usted ha eliminado correctamente el registro y se han devuelto los productos a su respectivo centro de costo");
    }

    /**
     * Display insumos list.
     *
     * @param  \App\Insumos  $insumos
     * @return \Illuminate\Http\Response
     */
    public function getStock(Request $request)
    {
        // $url = "http://10.4.237.8:3000/api/v1/PRODUCTOS/1";
        // $insumos = json_decode(file_get_contents($url), true);
        // $data = $insumos['data'];
        // return view('insumos/stock', compact('data'));
        return abort(403, 'No disponible por el momento.');
    }
    
    /**
     * Lista de Insumos para select2.
     *
     * @param  \App\Insumos  $insumos
     * @return \Illuminate\Http\Response
     */
    public function getProductoSelect(Request $request, WSIntersystem $client)
    {
        $data = $client->getProductos($request);
        return $data;
    }

    /**
     * Retorna Insumo solicitado.
     *
     * @param  \App\Insumos  $insumos
     * @return \Illuminate\Http\Response
     */
    public function getProducto(Request $request, WSIntersystem $client) {
        $data = $client->getProducto($request);
        return $data;
    }
    
    /**
     * Retorna Insumo del listado completo de pabellon.
     *
     * @param  \App\Insumos  $insumos
     * @return \Illuminate\Http\Response
     */
    public function getProductoCbInsumo(Request $request) {
        $data['results'] = CbInsumos::select('id as id',DB::raw("CONCAT(gl_nombre, ' (', gl_codigo_articulo, ')') as text"))
            ->where('gl_nombre', 'like', '%'.$request->search.'%')
            ->orWhere('gl_codigo_articulo', 'like', '%'.$request->search.'%')
            ->get();
        return $data;
    }
}
