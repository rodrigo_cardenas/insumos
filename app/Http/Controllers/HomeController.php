<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Insumos;
use Carbon\Carbon;
use App\Http\Controllers\Api\WSPaciente;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total = Insumos::all()->count();
        $ultimoAsignado = Insumos::with('insumoHermes')->get()->last();
        $insumosMasDemandado = Insumos::masdemandado()->first();
        $pacienteMasRepetido = Insumos::masrepetido()->first();

        return view('home', compact('total', 'insumosMasDemandado','pacienteMasRepetido','ultimoAsignado'));
    }

    /**
     * Obtiene paciente desde WS.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPaciente(Request $request, WSPaciente $client) 
    {
        return $client->getPaciente($request);
    }
}
