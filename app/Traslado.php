<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Traslado extends Model
{
	public $timestamps = false;
	
	protected $table = 'hos_movimiento';

    public $fillable = [
		'id_hospitalizacion',
		'id_ingreso',
		'id_ingreso_medico',
		'tx_tipo_movimiento',
		'fc_fecha_entrada',
		'id_unidad',
		'id_sala',
		'id_cama',
		'fc_fecha_salida',
		'nr_dias_estadia',
		'id_usuario',
		'id_servicio_emite'
	];
    

	public function servicio_emite()
	{
		return $this->belongsTo('App\Servicio', 'id_servicio_emite')->withDefault(["tx_descripcion" => "No asignado"]);
	}

	public function unidad()
	{
		return $this->belongsTo('App\Unidad', 'id_unidad')->withDefault(["tx_descripcion" => "No asignada"]);
	}

	public function sala()
	{
		return $this->belongsTo('App\Sala', 'id_sala')->withDefault(["tx_sala" => "No asignada"]);
	}

	public function cama()
	{
		return $this->belongsTo('App\Cama', 'id_cama')->withDefault(["cd_cama" => "No asignada"]);
	}
	
	public function hospitalizacion()
	{
		return $this->belongsTo('App\Hospitalizacion', 'id_hospitalizacion')->withDefault(["id" => "Error"]);
	}
	
    public function ingreso()
    {
		return $this->belongsTo('App\Ingreso', 'id_ingreso')->withDefault();
	}

	public function getFechaEntradaAttribute()
	{
		return ($this->fc_fecha_entrada != null) ? Carbon::createFromFormat('Y-m-d H:i:s', $this->fc_fecha_entrada)->format('d-m-Y H:i') : 'N/A';
	}

	public function getFechaSalidaAttribute()
	{
		return ($this->fc_fecha_salida != null) ? Carbon::createFromFormat('Y-m-d H:i:s', $this->fc_fecha_salida)->format('d-m-Y H:i') : 'N/A';
	}

	public function usuario()
	{
		return $this->belongsTo('App\User', 'id_usuario')->withDefault();
	}

	public function getAnteriorEsEgresoAttribute(){
		$previous = $this->where('id', '<', $this->id)->where('tx_tipo_movimiento', 'E')->where('id_hospitalizacion', $this->id_hospitalizacion)->max('id');
		return ($previous != null) ? true : false;
	}
	
	public function getAnteriorEsTosAttribute(){
		// $previous = $this->where('id', '<', $this->id)->where('tx_tipo_movimiento', 'TOS')->where('id_hospitalizacion', $this->id_hospitalizacion)->max('id');
		$previous = $this->where('id', '<', $this->id)->where('id_hospitalizacion', $this->id_hospitalizacion)->get()->last();
		// dd($previous);
		return (isset($previous->tx_tipo_movimiento) && $previous->tx_tipo_movimiento == 'TOS') ? true : false;
	}

	public function getAnteriorMovimientoAttribute(){
		$previous = $this->where('id', '<', $this->id)->where('id_hospitalizacion', $this->id_hospitalizacion)->get()->last();
		return $previous;
	}
	
	public function getUltimoMovimientoAttribute(){
		$last = $this->where('id_hospitalizacion', $this->id_hospitalizacion)->max('id');
		return ($last != null) ? $last : 0;
	}
}
