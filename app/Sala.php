<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sala extends Model
{
    protected $connection = 'mysql';
    protected $table = 'hos_sala';
    public $timestamps = false;

    public function unidad()
    {
        return $this->belongsTo('App\Unidad', 'id_unidad', 'id');
    }

    public function camas_disponibles(){
        return $this->hasMany('App\Cama', 'id_sala')->Where('id_estado_cama','!=', 4);
    }
    
    public function cama(){
        return $this->hasMany('App\Cama', 'id_sala', 'id');
    }

    public function movimientos()
    {
        return $this->hasMany('App\Traslado', 'id_sala');
    }

    public function ingresos()
    {
        return $this->hasMany('App\Ingreso', 'tx_sala');
    }

    public function preHospitalizaciones()
    {
        return $this->hasMany('App\PreHospitalizacion', 'id_sala');
    }

}
