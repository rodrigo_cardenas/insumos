<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Procedimiento extends Model
{
    protected $table = 'procedimientos';
    public $guarded = [];

    public static function boot()
    {
        parent::boot();
       
        static::creating(function ($curacion) {
            $curacion->created_by = Auth::id();
        });
        
        static::created(function ($curacion) {
            $curacion->created_by = Auth::id();
        });
    }

    public function paquetes()
    {
		return $this->hasMany('App\Paquetes', 'id_procedimiento');
	}
}
