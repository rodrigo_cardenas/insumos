<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paquetes extends Model
{
    protected $table = 'paquetes';
    public $guarded = [];
    public $timestamps = false;

    public function procedimiento()
    {
		return $this->belongsTo('App\Procedimiento', 'id_procedimiento')->withDefault();
    }
    
    public function insumo()
    {
		return $this->belongsTo('App\CbInsumos', 'id_insumo')->withDefault();
    }
}
