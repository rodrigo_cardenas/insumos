<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Hospitalizacion;
use App\SolicitudPabellon;
use Auth;
use Session;

class Paciente extends Model 
{
	protected $connection = 'mysql3';
    protected $table = 'generales.gen_paciente';
    protected $appends = ['edad', 'nombre_completo', 'rut', 'rut_sin_puntos', 'fecha_nacimiento'];


    public static function boot()
    {
       parent::boot();
       
        //evento que se dispara cuando se hace un update para registrar usuario (id gen_users) que modifica el registro      
        static::saving(function($paciente)
        {
           $paciente->updated_by = Session::get('id_user_portal');
        });
       
    }

  
    // convierte a mayusculas todo string
    public function setAttribute($key, $value)
    {
        parent::setAttribute($key, $value);
        if (is_string($value)){
            $this->attributes[$key] = trim(mb_strtoupper($value));
        }
    }

    public $fillable = [
        "id_tipo_dentificacion_paciente",
        "nr_run",
        "tx_digito_verificador",
        "tx_pasaporte",
        "nr_ficha",
        "tx_apellido_paterno",
        "tx_apellido_materno",
        "tx_nombre",
        "id_sexo",
        "fc_nacimiento",
        "id_estado_paciente",
        "id_prevision",
        "id_clasificacion_fonasa",
        "fc_creacion",
        "tx_direccion",
        "id_pais",
        "id_comuna",
        "id_pueblo_originario",
        "id_estado_civil",
        "bo_estado",
        "updated_by",
        "id_usuario"
    ];

    public function getEdadAnosAttribute(){
        if ($this->fc_nacimiento != null) {
            $f1 = Carbon::createFromFormat('Y-m-d', $this->fc_nacimiento);
            $f2 = Carbon::now();
            return $f2->diff($f1)->y;
        }
        return false;
    }
    
    public function getEdadAttribute(){
        if ($this->fc_nacimiento != null) {
            $f1 = Carbon::createFromFormat('Y-m-d', $this->fc_nacimiento);
            $f2 = Carbon::now();
            $anos = $f2->diff($f1)->format('%y años');
            // solo si es menor de 3 años mostrar años con meses y días
            if ($f2->diff($f1)->y < 3) {
                $dias = $f2->diff($f1)->format('%d días');
                $meses = $f2->diff($f1)->format('%m meses');
                $anos = $f2->diff($f1)->format('%y años');

                if($f2->diff($f1)->format('%d') == '1')
                    $dias = $f2->diff($f1)->format('%d día');
                if($f2->diff($f1)->format('%m') == '1')
                    $meses = $f2->diff($f1)->format('%m mes');
                if($f2->diff($f1)->format('%y') == '1')
                    $anos = $f2->diff($f1)->format('%y año');

                if ($f2->diff($f1)->y == 0) {
                    if ($f2->diff($f1)->format('%m') == '0'){
                        return $dias;
                    } 
                    return "{$meses} y {$dias}";
                }
                
                return "{$anos} con {$meses} y {$dias}";
            }
            return $anos;
        }
        return false;
    }

    public function getRutAttribute()
    {
        if ($this->nr_run == 0 || $this->nr_run == null): return "Sin Información"; endif;
        $nr_run = formatCLP($this->nr_run);
        return "{$nr_run}-{$this->tx_digito_verificador}";
    }

    public function getRutSinPuntosAttribute()
    {
        return str_replace(".", "", $this->rut);
    }

    public function getNombreCompletoAttribute()
    {
        return "{$this->tx_nombre} {$this->tx_apellido_paterno} {$this->tx_apellido_materno}";
    }

    public function getApellidosAttribute()
    {
        return "{$this->tx_apellido_paterno} {$this->tx_apellido_materno}";
    }

    public function getUltimaHospitalizacionAttribute()
    {
        $hosp = $this->hospitalizaciones->last();
        return $hosp != null ? $hosp : null;
    }
    
    public function getUltimoMovimientoAttribute()
    {
        $hosp = Hospitalizacion::where("id_paciente", $this->id)->whereNull("fc_egreso_hospitalizacion")->get()->last();
        $movimiento = isset($hosp->id) ? Traslado::with(['sala', 'cama', 'unidad'])->where('id_hospitalizacion', $hosp->id)->get()->last() : null;
        return $movimiento != null ? $movimiento : null;
    }
    
    public function getUltimaSolicitudGineAttribute()
    {
        $solicitud = $this->solicitudGine->last();
        return isset($solicitud->id) ? $solicitud : null;
    }

    public function getEstadoHospitalizacionAttribute()
    {
        return $this->tiene_hospitalizacion_activa ? "Paciente Hospitalizado" : "Paciente no hospitalizado";
    }

    public function getTieneHospitalizacionActivaAttribute()
    {
        $hosp = $this->ultima_hospitalizacion;
        return isset($hosp) ? $hosp->fc_egreso_hospitalizacion == null : false;
    }

    public function getFechaNacimientoAttribute()
    {
        return $this->fc_nacimiento != null ? Carbon::createFromFormat('Y-m-d', $this->fc_nacimiento)->format('d-m-Y') : null;
    }

    public function getDireccionAttribute()
    {
        return $this->tx_direccion;
    }

    public function getFichaAttribute()
    {
        return $this->nr_ficha;
    }

    public function getSolicitudAmbulanciaActivaAttribute()
    {
        $ultimaHospitalizacion = $this->ultima_hospitalizacion;
        if ($ultimaHospitalizacion != null) {
            $ultimaSolicitudAmbulancia = $this->solicitudes_ambulancia->last(); 
            return $ultimaSolicitudAmbulancia != null ? $ultimaSolicitudAmbulancia->id_hospitalizacion == $ultimaHospitalizacion->id : false;
        } else {
            return false;
        }
        // $solcitudCreada = isset($hosp->id) ? SolicitudAmbulancia::where('bo_estado_solicitud', 1)->get()->last() : null;
        // return $solicitudCreada != null ? $solicitudCreada : null;
    }

    public function prevision()
    {
		return $this->belongsTo('App\Prevision', 'id_prevision')->withDefault(["tx_descripcion" => "Sin Información"]);
    }
    
    public function clasificacion_fonasa()
    {
		return $this->belongsTo('App\ClasificacionFonasa', 'id_clasificacion_fonasa')->withDefault(["tx_descripcion" => "Sin Información"]);
    }
    
    public function pais()
    {
		return $this->belongsTo('App\Pais', 'id_pais')->withDefault(["tx_descripcion" => "Sin Información"]);
    }
    
    public function sexo()
    {
		return $this->belongsTo('App\Sexo', 'id_sexo')->withDefault(["tx_descripcion" => "Sin Información"]);
    }
    
    public function comuna()
    {
		return $this->belongsTo('App\Comuna', 'id_comuna')->withDefault([
            "cd_comuna" => "Sin información de comuna", "tx_descripcion" => "Sin información de la comuna"]);
    }
    
    public function pueblo_originario()
    {
		return $this->belongsTo('App\Etnia', 'id_pueblo_originario')->withDefault(["tx_descripcion" => "Ninguno"]);
    }
    
    public function estado_civil()
    {
		return $this->belongsTo('App\EstadoCivil', 'id_estado_civil')->withDefault(["tx_descripcion" => "Desconocido"]);
	}
    
    public function usuario()
    {
		return $this->belongsTo('App\User', 'id_usuario')->withDefault(["name" => "Sin Información"]);
    }
    
    public function pacienteZscore()
    {
		return $this->hasMany('App\PacienteZscore', 'id_paciente');
    }

    public function ingreso_medico()
    {
        return $this->hasMany('App\IngresoMedico', 'id_paciente');
    }
    
    public function solicitudGine()
    {
        return $this->hasMany('App\SolicitudGine', 'id_paciente');
    }

    public function hospitalizaciones()
    {
        return $this->hasMany('App\Hospitalizacion', 'id_paciente');
    }

    public function solicitudes_ambulancia()
    {
        return $this->hasMany('App\SolicitudAmbulancia', 'id_paciente');
    }

    public function oirs()
    {
        return $this->hasMany('App\ObservacionesOirs', 'id_paciente');
    }

    public function epicrisis_medica()
    {
        return $this->hasMany('App\EpicrisisMedica', 'id_paciente');
    }
    
    public function epicrisis_medica_old_general()
    {
        return $this->hasManyThrough(
            'App\EpicrisisMedicaOldGeneral', 
            'App\PacientNcr',
            'PAC_RUT',
            'id_paciente',
            'nr_run',
            'PAC_CORR'
        );
    }
    
    public function epicrisis_medica_old_vascular()
    {
        return $this->hasManyThrough(
            'App\EpicrisisMedicaOldVascular', 
            'App\PacientNcr',
            'PAC_RUT',
            'id_paciente',
            'nr_run',
            'PAC_CORR'
        );
    }

    public function getSolicitudesPabellonAttribute()
    {
        return SolicitudPabellon::with('protocolos')->where('gl_rut', "{$this->rut_sin_puntos}")->whereIn('id_pabellon', [1, 2, 3])->get();
    }
}
