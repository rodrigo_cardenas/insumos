@extends('adminlte::page')

@section('title', ' | Registrar Procedimiento')

@section('content_header')
    {{-- <h1>Registrar Insumos por Paciente</h1> --}}
@stop

@section('css')
<link href="{{ asset('css/customHSJD.css') }}" rel="stylesheet">
@stop

@section('content')
    <div id="app">
        <div class="box box-info insumos">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ url('procedimientos') }}" method="post">
                            @csrf
                            <div class="row form-group">
                                <label for="tipo_atencion_1" class="col-sm-2 control-label">Nombre Procedimiento</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="tx_descripcion" name="tx_descripcion" placeholder="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Agregar Insumos <button id="nuevoProducto" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Agregar Producto</button></h3>
                                    <br>
                                    <div class="row form-group producto" id="producto">
                                        <label for="tipo_atencion_1" class="col-sm-2 control-label"><i class="fa fa-pills"></i> Buscar Producto</label>
                                        <div class="col-sm-6">
                                            <select class="select_insumos select2" name="insumos[]" id="insumos" required>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="cantidad[]" id="" placeholder="Cantidad" required>
                                        </div>
                                        <div class="col-md-1">
                                            <a class="btn btn-danger close-div"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div id="alert-div"></div>
                                <button type="submit" id="btn-submit" class="btn btn-primary">Guardar</button>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
       
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    var url_insumos = @json(url('insumos/getProductoCbInsumo'));
    
    $('.select_insumos').select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Ingrese Nombre o Código",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_insumos,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    });
    $("#nuevoProducto").on('click', function(){
        var div = $(".producto"); 

        //find all select2 and destroy them   
        div.find(".select2").each(function(index)
        {
            if ($(this).data('select2')) {
                $(this).select2('destroy');
            } 
        });

        //Now clone you select2 div 
        $('.producto:last').clone( true).insertAfter(".producto:last"); 

        //we must have to re-initialize  select2 
        $('.select2:last').val(null).trigger('change');
        $('.select2').select2({
            width: '99%',
            allowClear: true,
            minimumInputLength: 3,
            placeholder: "Ingrese Nombre o Código",
            language: {
                inputTooShort: function() {
                    return 'Ingrese 3 o más caracteres para la búsqueda';
                }
            },
            ajax: {
                url: url_insumos,
                // dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    // Query parameters will be ?search=[term]&type=public
                    return query;
                }
            }
        });
    });

    // eliminar producto
    $('.close-div').on('click', function(){
        $(this).closest("#producto").remove();
    });

  
    function setIngreso(id_ingreso, id_unidad) {
        $('.box-widget').css('background-color','white');
        $('#id_ingreso').val(id_ingreso);
        $('#id_unidad').val(id_unidad);
        var url_insumos = @json(url('insumos/getProductoCbInsumo'));
        console.log(id_unidad);
        $('.select_insumos').select2({
            width: '99%',
            allowClear: true,
            minimumInputLength: 3,
            placeholder: "Ingrese Nombre o Código",
            language: {
                inputTooShort: function() {
                    return 'Ingrese 3 o más caracteres para la búsqueda';
                }
            },
            ajax: {
                url: url_insumos,
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                }
            }
        });
    }

</script>
@stop