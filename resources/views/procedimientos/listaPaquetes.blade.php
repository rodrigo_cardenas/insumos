@forelse ($procedimientos->paquetes as $paquete)
    <div class="row form-group producto" id="producto">
        <label for="tipo_atencion_1" class="col-sm-2 control-label"><i class="fa fa-pills"></i> Buscar Producto</label>
        <div class="col-sm-6">
            <select class="select_insumos select2" name="insumos[]" id="insumos" readonly required>
                <option value="{{ $paquete->insumo->gl_codigo_articulo }}" selected>{{ $paquete->insumo->gl_nombre }}</option>
            </select>
        </div>
        <div class="col-md-2">
            <input type="number" class="form-control" name="cantidad[]" id="" value="{{ $paquete->cantidad }}" placeholder="Cantidad" required>
        </div>
        <div class="col-md-1">
            <a class="btn btn-danger close-div"><i class="fa fa-times"></i></a>
        </div>
    </div>
@empty
    
@endforelse