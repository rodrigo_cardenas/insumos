@extends('adminlte::page')

@section('title', ' | Procedimientos')

@section('content_header')
@stop

@section('content')

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('error'))
    <div class="alert alert-danger">
        <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
    </div>
@endif

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Mantenedor Procedimientos</h3>

        <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table no-margin" id="procedimientos"></table>
        </div>
    </div>
    <div class="box-footer clearfix">
        <a href="{{ action('ProcedimientoController@create') }}" class="btn btn-sm btn-info btn-flat pull-left">Registrar Nuevo Procedimiento</a>
    </div>
</div>
@stop
@section('js')
<script>
    var procedimientos = @json($procedimientos);

    $('#procedimientos').DataTable( {
        order: [],
        "language": {"url": "{{url('/')}}/js/plugins/dataTables/Spanish.json"},
        "data": procedimientos,
        "columns": [
            {"title": "ID", "data": "id" },
            {"title": "Procedimiento", "data": "tx_descripcion" },
            {"title": "Articulos", "data": "paquetes[<br>].insumo.gl_nombre" },
            {"title": "", "data": "id", render: function (dataField) { 
                    return `
                        <a title="Editar" class="btn btn-xs btn-white hidden" href="/procedimientos/update/${dataField}"><i class="fa fa-edit"></i></a>
                        <form action="{{ url('/procedimientos') }}/${dataField}" method="post">
                            @csrf
                            @method('delete')
                            <button class="btn btn-xs btn-white" title="Eliminar" onclick="return confirm('Estas seguro de eliminar esta curación?')" type="submit"><i class="fa fa-trash"></i></button>
                        </form>	
                    `; 
                } 
            }
        ]
    } ); 

    $(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });
    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });
</script>
@stop
