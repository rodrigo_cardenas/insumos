@extends('adminlte::page')

@section('title', ' | Registrar Insumos')

@section('content_header')
    {{-- <h1>Registrar Insumos por Paciente</h1> --}}
@stop

@section('css')
<link href="{{ asset('css/customHSJD.css') }}" rel="stylesheet">
@stop

@section('content')
    <div id="app">
        <div class="box box-info paciente">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-8">
                        <form id="buscarPaciente" action="javascript:;">
                            <div class="row form-group">
                                <label for="tipo_atencion_1" class="col-sm-4 control-label"><i class="fa fa-barcode"></i> Ingresar Código de Barras</label>
                                <div class="col-sm-3">
                                    <input type="number" class="form-control" id="codigoBarra" name="codigoBarra" placeholder="">
                                </div>
                            </div>
                            <div class="row form-group">
                                <p id="rutHelp" class="col-sm-6">
                                    <span class="fa fa-exclamation-circle" style="color:red"></span>
                                    <b><i>También Puedes buscar por Rut o Ficha </i></b>
                                </p>
                            </div>
    
                            <div class="row form-group">
                                <label for="rut" class="col-sm-4 control-label" >Rut </label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="rut" name="rut" placeholder="Ej: 11222333-K">
                                </div>
                            </div>
                            
                            <div class="row form-group">
                                <label for="ficha" class="col-sm-4 control-label" >Ficha </label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" value="{{ $ficha }}" id="ficha" name="ficha" placeholder="Ej: 123456789">
                                </div>
                            </div>
                            <div class="box-footer">
                                <div id="alert-div"></div>
                                <button type="submit" id="btn-submit" class="btn btn-primary">Buscar</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4 left">
                        <div class="box box-widget widget-user">
                            <div class="widget-user-header bg-aqua">
                                {{-- <div class="widget-user-image">
                                    <img class="img-circle" src="/img/paciente.png" alt="User Avatar">
                                </div> --}}
                                <h3 class="widget-user-username"><strong><span class="nombreP">Datos Paciente</span></strong></h3>
                                <h5 class="widget-user-desc" id="prevision"></h5>
                            </div>
                            <div class="box-footer no-padding">
                                <ul class="nav nav-stacked">
                                <li><a href="#">Rut: <span class="rutP"></span></a></li>
                                <li><a href="#">Ficha: <span class="fichaP"></span></a></li>
                                <li><a href="#">F. Nac.: <span class="fechaNacP"></span></a></li>
                                <li id="box_ingresos">
                                    {{-- hospitalizaciones e ingresos --}}
                                </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="box box-info insumos">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Agregar Insumos 
                            <small>
                                <select name="paquetes" id="paquetes" class="pull-right">
                                    <option value=""></option>
                                </select>
                            </small> 
                            <button id="nuevoProducto" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Agregar Producto</button>
                        </h3>
                        <br>
                        <form action="{{ action('InsumosController@store') }}" method="post">
                            @csrf
                            <input type="hidden" name="id_paciente" id="id_paciente">
                            <input type="hidden" name="ficha" id="ficha">
                            <input type="hidden" name="nr_run" id="nr_run">
                            <input type="hidden" name="id_ingreso" id="id_ingreso">
                            <input type="hidden" name="id_unidad" id="id_unidad">
                            <div id="divPaquete"></div> 
                            <div class="row form-group producto" id="producto">
                                <label for="tipo_atencion_1" class="col-sm-2 control-label"><i class="fa fa-pills"></i> Buscar Producto</label>
                                <div class="col-sm-6">
                                    <select class="select_insumos select2" name="insumos[]" id="insumos" required>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <input type="number" class="form-control" name="cantidad[]" id="" placeholder="Cantidad" required>
                                </div>
                                <div class="col-md-1">
                                    <a class="btn btn-danger close-div"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <button class="btn btn-success" onclick="return confirm('Estas seguro de guardar estos productos?');" type="submit">Confirmar Productos</button>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    $('#codigoBarra').focus();
    $('#codigoBarra').change(function () {
        if ( $( "#rut" ).val() === "" && $( "#ficha" ).val() === "" ) {
            $( "#alert-div" ).html( '<div class="alert alert-danger" role="alert">Debe ingresar al menos ficha o rut</div>' ).show().fadeOut( 5000 );
            event.preventDefault();
            return
        }
        $.get( "{{ url('getPaciente') }}?rut="+$( "#rut" ).val()+"&ficha="+$( "#ficha" ).val(), function( json ) {
            if (!json.error) {
                console.log(json.data[0].detalleHospitalizacion.hospitalizaciones);
                $( ".datosPaciente" ).css( "display", "" );
                $( ".brazaletePaciente" ).css( "display", "" );
                $( ".alert-div" ).html( "" );
                $( ".nombreP" ).html(json.data[0].nombre_paciente + " " + json.data[0].apellidop_paciente + " " + json.data[0].apellidom_paciente);
                $( ".rutP" ).html(json.data[0].rut_paciente );
                $( ".fichaP" ).html(json.data[0].no_ficha );
                $( ".fechaNacP" ).html(json.data[0].fn_paciente + " ("+ json.data[0].edad + ")");
                $( "#id_paciente" ).val(json.data[0].id );
                $( "#ficha" ).val(json.data[0].no_ficha );
                $( "#nr_run" ).val(json.data[0].rut_paciente );
                $( "#prevision" ).html(json.data[0].Prevision + " " + json.data[0].Plan);
                $.each( json.data[0].detalleHospitalizacion.hospitalizaciones, function ( index, user ) {
                    console.log(user);
                    $('#box_ingresos').append(`
                    <div class="box box-widget collapsed-box">
                                        <div class="box-header with-border">
                                            <div class="user-block">
                                                <img class="img-circle" src="/img/paciente.png" alt="User Image">
                                                <span class="username"><a href="#" id="sector_hosp">Sector.</a></span>
                                                <span class="estado_hosp">Shared publicly - 7:30 PM Today</span>
                                            </div>
                                            <div class="box-tools">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-body" style="display: none;">
                                          <p>hola</p>
                                        </div>
                                    </div>
                    `);
                });
            }else{
                $( "#alert-div" ).html( '<div class="alert alert-danger" role="alert">'+ json.message +'</div>' ).show().fadeOut( 5000 );
                event.preventDefault();
            }
        }, "json");
        
        return;
    })
    var url_insumos = @json(url('insumos/getProductoSelect'))+'?centroCosto=';
    $('.select_insumos').select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Ingrese Nombre o Código",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_insumos,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    });
    $("#nuevoProducto").on('click', function(){
        var div = $(".producto"); 

        //find all select2 and destroy them   
        div.find(".select2").each(function(index)
        {
            if ($(this).data('select2')) {
                $(this).select2('destroy');
            } 
        });

        //Now clone you select2 div 
        $('.producto:last').clone( true).insertAfter(".producto:last"); 

        //we must have to re-initialize  select2 
        $('.select2:last').val(null).trigger('change');
        $('.select2').select2({
            width: '99%',
            allowClear: true,
            minimumInputLength: 3,
            placeholder: "Ingrese Nombre o Código",
            language: {
                inputTooShort: function() {
                    return 'Ingrese 3 o más caracteres para la búsqueda';
                }
            },
            ajax: {
                url: url_insumos+$('#id_unidad').val(),
                // dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    // Query parameters will be ?search=[term]&type=public
                    return query;
                }
            }
        });
    });

    // eliminar producto
    $('.close-div').on('click', function(){
        $(this).closest("#producto").remove();
    });

    $( "#buscarPaciente" ).submit(function( event ) {
        
        if ( $( "#rut" ).val() === "" && $( "#ficha" ).val() === "" ) {
            $( "#alert-div" ).html( '<div class="alert alert-danger" role="alert">Debe ingresar al menos ficha o rut</div>' ).show().fadeOut( 5000 );
            event.preventDefault();
            return
        }
        $.get( "{{ url('getPaciente') }}?rut="+$( "#rut" ).val()+"&ficha="+$( "#ficha" ).val(), function( json ) {
            if (!json.error) {
                console.log(json);
                $( ".datosPaciente" ).css( "display", "" );
                $( ".brazaletePaciente" ).css( "display", "" );
                $( ".alert-div" ).html( "" );
                $( ".nombreP" ).html(json.data[0].nombre_paciente + " " + json.data[0].apellidop_paciente + " " + json.data[0].apellidom_paciente);
                $( ".rutP" ).html(json.data[0].rut_paciente );
                $( ".fichaP" ).html(json.data[0].no_ficha );
                $( ".fechaNacP" ).html(json.data[0].fn_paciente + " ("+ json.data[0].edad + ")");
                $( "#id_paciente" ).val(json.data[0].id );
                $( "#prevision" ).html(json.data[0].Prevision + " " + json.data[0].Plan);
                $( "#ficha" ).val(json.data[0].no_ficha );
                $( "#nr_run" ).val(json.data[0].rut_paciente );
                $('#box_ingresos').html('');
                $.each( json.data[0].detalleHospitalizacion.hospitalizaciones.reverse(), function ( index, hospitalizacion ) {
                    $.each( hospitalizacion.ingresos.reverse(), function ( index, ingreso ) {
                        console.log(ingreso);
                        $('#box_ingresos').append(`
                            <div class="box box-widget collapsed-box" onclick="setIngreso(${ingreso.id}, ${ingreso.movimientos[ingreso.movimientos.length-1].id_unidad}); $(this).css('background-color','aliceblue');">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="/insumos/img/paciente.png" alt="User Image">
                                        <span class="username"><a href="#" id="sector_hosp">${ (ingreso.tx_sector) ? ingreso.unidad.tx_descripcion : hospitalizacion.servicio_egreso }</a></span>
                                        <span class="estado_hosp">Fecha Ingreso Servicio: ${ingreso.fecha_ingreso}</span>
                                        <br>
                                        <span class="estado_hosp">Estado Hosp: ${hospitalizacion.estado_hospitalizacion} - ${hospitalizacion.fecha_egreso}</span>
                                    </div>
                                </div>
                            </div>
                        `);
                    });
                });
            }else{
                $( "#alert-div" ).html( '<div class="alert alert-danger" role="alert">'+ json.message +'</div>' ).show().fadeOut( 5000 );
                event.preventDefault();
            }
        }, "json");
        
        return;
    });

    function setIngreso(id_ingreso, id_unidad) {
        $('.box-widget').css('background-color','white');
        $('#id_ingreso').val(id_ingreso);
        $('#id_unidad').val(id_unidad);
        var url_insumos = @json(url('insumos/getProductoSelect'))+'?centroCosto='+$('#id_unidad').val();
        console.log(id_unidad);
        $('.select_insumos').select2({
            width: '99%',
            allowClear: true,
            minimumInputLength: 3,
            placeholder: "Ingrese Nombre o Código",
            language: {
                inputTooShort: function() {
                    return 'Ingrese 3 o más caracteres para la búsqueda';
                }
            },
            ajax: {
                url: url_insumos,
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                }
            }
        });
    }

    var procedimientos = @json($procedimientos);
    $('#paquetes').select2({
        data: procedimientos,
        width: '30%',
        allowClear: true,
        placeholder: "Importar paquete desde procedimiento",
    });

    $('#paquetes').change(function () {
        $.get( "{{ url('getPaquete') }}/"+this.value , function( json ) {
            $("#divPaquete").html(json);
            // eliminar producto
            $('.close-div').on('click', function(){
                $(this).closest("#producto").remove();
            });
            // Inicializar select2
            $('.select_insumos').select2({
                width: '99%',
            });
        });
        return;
    })

</script>
@stop