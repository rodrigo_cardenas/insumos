@extends('adminlte::page')

@section('title', ' | Dashboard')

@section('content_header')
@stop

@section('content')

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Insumos</h3>

        <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table no-margin" id="resumen">
                <thead>
                <tr>
                    <th>CODIGO</th>
                    <th>DESCRIPCION</th>
                    <th>BODEGA</th>
                    <th>SUB_BODEGA</th>
                    <th>PMP</th>
                    <th>STOCK</th>
                </tr>
                </thead>
            
            </table>
        </div>
    </div>
    <div class="box-footer clearfix">
        <a href="{{ action('InsumosController@create') }}" class="btn btn-sm btn-info btn-flat pull-left">Registrar Nuevos Insumos</a>
        {{-- <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a> --}}
    </div>
</div>
@stop
@section('js')
<script>
var insumos = @json($data);
$("#resumen").DataTable().destroy();
$('#resumen').DataTable( {
    order: [],
    "language": {"url": "{{url('/')}}/js/plugins/dataTables/Spanish.json"},
    "data" : insumos,
    "columns": [
        { "data": "CODIGO" },
        { "data": "DESCRIPCION" },
        { "data": "BODEGA" },
        { "data": "SUB_BODEGA" },
        { "data": "PMP" },
        { "data": "STOCK" }
    ]
} );
</script>
@stop
