@extends('adminlte::page')

@section('title', ' | Dashboard')

@section('content_header')
@stop

@section('content')
<div class="row">
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
           <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-tasks"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">Total asignados</span>
            <span class="info-box-number">{{ $total }}<small></small></span>
        </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-tablets"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">Más Demandados</span>
            <span class="info-box-number"><small>{{ mb_strimwidth($insumosMasDemandado->insumoHermes->gl_nombre, 0, 30, '...') }}</small>({{ $insumosMasDemandado->cantidadTotal }})</span>
        </div>
        </div>
    </div>

    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-pills"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">Último asignado</span>
            <span class="info-box-number"><small>{{ mb_strimwidth($ultimoAsignado->insumoHermes->gl_nombre, 0, 39, '...') }}</small></span>
        </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-user-injured"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">Top Paciente Insumos</span>
            <span class="info-box-number"><small>{{ $pacienteMasRepetido->paciente->nombre_completo }}</small></span>
        </div>
        </div>
    </div>
</div>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Últimos insumos asignados</h3>

        <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table no-margin" id="resumen">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Item</th>
                    <th>Cantidad</th>
                    <th>Unidad</th>
                    <th>Paciente</th>
                    <th>Unidad</th>
                    <th>Fecha Registro</th>
                    <th>Usuario</th>
                    <th></th>
                </tr>
                </thead>
            
            </table>
        </div>
    </div>
    <div class="box-footer clearfix">
        <a href="{{ action('InsumosController@create') }}" class="btn btn-sm btn-info btn-flat pull-left">Registrar Nuevos Insumos</a>
        {{-- <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a> --}}
    </div>
</div>
@stop
@section('js')
<script>
$("#resumen").DataTable().destroy();
$('#resumen').DataTable( {
    order: [],
    "language": {"url": "{{url('/')}}/js/plugins/dataTables/Spanish.json"},
    "ajax": "{{ url('insumos') }}",
    "columns": [
        { "data": "id" },
        { "data": "insumo_hermes.gl_nombre" },
        { "data": "cantidad" },
        { "data": "insumo_hermes.gl_unidad" },
        { "data": "paciente.nombre_completo" },
        { "data": "ingreso.hospitalizacion.unidad.tx_descripcion" },
        { "data": "fecha_creacion" },
        { "data": "usuario.name" },
        { "data": "id", render: function (dataField) { 
                return '<a title="Eliminar" onclick="return confirm('+"'Estas seguro de eliminar esta curación?'"+');" class="btn btn-xs btn-white" href="/insumos/destroy/' + dataField + '"><i class="fa fa-trash"></a>'; 
            } 
        }
    ]
} );

$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
    $(".alert-success").slideUp(1000);
});
$(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
    $(".alert-danger").slideUp(1000);
});
</script>
@stop
